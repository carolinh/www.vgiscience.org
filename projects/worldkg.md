---
layout: page
---

# WorldKG: World-Scale Completion of Geographic Knowledge

**Dr. Elena Demidova, L3S Research Center, Hannover**

OpenStreetMap (OSM) is a rich source of openly available volunteered geographic information. OSM is adopted by a variety of real-world applications on the Web and beyond, including online route planners and geographic information sites. However, representations of geographic entities in OSM are highly diverse and incomplete, being restricted to few mandatory properties and heterogeneous tags, i.e. user-defined key-value pairs.

Recently emerged knowledge graphs (i.e. graph-based knowledge repositories) such as Wikidata, EventKG and DBpedia provide a rich source of contextual information about geographic entities and support semantic queries. For example, in September 2018 Wikidata contained more than 6.4 million entities including locations, points of interest, mountain peaks and others.

Whereas knowledge graphs provide a wide range of complementary semantic information for geographic entities, highly useful for Web applications, identity links between OSM and knowledge graphs are still rare and are mainly manually defined by volunteers.

The problem of link discovery for OSM datasets is particularly challenging due to the large scale, richness, heterogeneity and different localisation.
The main goal of the WordKG project is to facilitate world-scale interlinking of OSM datasets describing different geographic regions with knowledge graphs as well as completion of spatial knowledge in the knowledge graphs using OSM data.

This goal is translated into three main objectives:
1. Development of methods for world-scale alignment of OSM datasets and knowledge graphs at the schema and instance level.
2. Development of methods for VGI-based knowledge graph completion through instance fusion and creation of new semantic instances based on VGI information.
3. Preparation and release data resulting from these methods in form of the WorldKG knowledge graph that provides comprehensive semantic representation of geographical information and its context.

The results of the WorldKG project can substantially benefit applications in mobility, transportation, tourism and logistics domains, as well as provide a basis for informational maps and services through high quality semantic representation of geographical and contextual information. 
 

