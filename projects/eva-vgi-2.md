---
layout: page
---

# Geovisual analysis of VGI for understanding people's behaviour in relation to multi-faceted context (EVA-VGI 2)

**Professor Dr.-Ing. habil. Dirk Burghardt**  
*Technische Universität Dresden, Institut für Kartographie, 01062 Dresden*

**Professor Dr. Stefan Wrobel**  
*Rheinische Friedrich-Wilhelms-Universität Bonn, Institut für Informatik III, Römerstraße 164, 53117 Bonn | Director of Fraunhofer Institute IAIS*

**Professor Dr. Ross Purves**  
*Universität Zürich, Geographisches Institut*

Volunteered Geographic Information (VGI) in the form of actively and passively generated spatial content offers extensive potential for a wide range of applications. Realising this potential however requires methods which take account of the specific properties of such data, for example its heterogeneity, quality, subjectivity, spatial resolution and temporal relevance. The creation and production of such content through social media platforms is an expression of human behaviour, and as such influenced strongly by events and external context. In this project we will develop geovisual analysis methods which show how actors interact in LBSM, and how their interactions influence, and are influenced by, their physical and social environment and relations.

![](../images/eva-vgi-2-fig-1.jpg)  
*Visual analytics system supporting exploration of evolution of the popularity of different topics and keywords in social media across geographic locations and users (Chen et al. 2018).*

In the [first phase of the project](eva-vgi.html), we developed and demonstrated a conceptual model enabling the extraction, analysis and visualisation of events and reactions to events in location based social media (LBSM). A central element of this model and its implementation is the integration of spatial, temporal, thematic and social dimensions combined with an explicit link between events and reactions. The follow up project will concentrate on the development of methods allowing links to be made between reactions, human activities and emotions. Furthermore, the social and physical context of events, and the resulting influences on and from behaviour will form a central research element. The project will explore the representativity of LBSM and its limits with respect to a range of research questions and application fields. An important focus will be on the identification of relationships between different actors and events, supported by the use and further development of a broad palette of methods and workflows from the domain of visual analytics.

As well as the development of generic approaches enabling comparative analysis using geostatistical and visual methods, we will refine, combine and parameterise methods for specific application domains. The focus here lies in three thematic areas to which the project consortium can bring experience and contact to partners from practice: 1) landscape and urban planning for the analyse and evaluation of green areas, 2) transport planning to explore group and individual mobility patterns and 3) political science through the application of comparative visual methods.
