---
layout: post
title: "Call for proposals"
date: "2018-07-05 16:00:33 +0200"
---

## Priority Programme “Volunteered Geographic Information: Interpretation, Visualisation and Social Computing“ (SPP 1894)

`No. 32`

The Senate of the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation) has established a Priority Programme entitled “Volunteered Geographic Information: Interpretation, Visualisation and Social Computing” (SPP 1894). The programme has been started in 2016 and is designed to run for six years. Applications are now invited for the second three-year period of this Priority Programme.

During the last years the availability of spatial data has rapidly developed, in particular through the diffusion of social networks, Web 2.0 platforms and availability of suitable sensor technologies. Characteristic of this development is the involvement of a large number of users, who, often using smart phones and mobile devices, generate and make freely available Volunteered Geographic Information (VGI), in the broader sense user generated spatial information.

The specific potential of this new information source is based on the characteristics of the underlying data, such as real-time availability, event-driven generation, and subjectivity, all with an implicit or explicit spatial reference. For the information society, these data can support a variety of applications for the solution of grand societal challenges e.g. in the fields of environment and disaster management, health, transport or citizen participation.

This priority programme aims to provide a scientific basis for raising the potential of VGI- and sensor data. Three main research domains are especially relevant for the advancement of VGI, namely “Information Retrieval and Analysis of VGI”, “Geovisualisation and user interactions related to VGI” and “Active participation, social context and privacy awareness”. Methodological research is required within these three domains, where the following sub-topics are of particular interest:

#### Research Domain “Information Retrieval and Analysis of VGI”
* Information extraction (space, time, semantics)
* Data aggregation and fusion of different sources and space/time scales
* Machine learning and algorithmic interpretation for VGI
* Analytics of mobility patterns and social routing
* Quality assessment and uncertainty analysis of VGI

#### Research Domain “Geovisualisation and user interactions related to VGI”
* Real-time visualisation and spatio-temporal analysis of data streams
* Geovisual analytics of location-based social media/networks
* Scalable visualisations, abstraction and multi-scale views related to VGI
* Cartographic communication in relation to VGI
* Impact of cognition principles on the use of VGI-based maps
* VGI platforms and interfaces

#### Research Domain “Active participation, social context and privacy awareness”
* Social context dependent data capture, use and dissemination
* Behavioural aspects of providing/sharing VGI (motivation, participation, trustworthiness)
* Information management and decision analysis based on VGI data
* Analysis of digital volunteer communities
* Privacy preserving methods of VGI

The fundamental research potential especially emerges at the interfaces of the three domains, thus, in the proposals, interactions and interdependencies should be taken into account. Therefore, applicants are expected to address at least two of the above research domains, and explain relevance and synergies resulting from this connection. For this second phase it is strongly encouraged to collaborate with projects within the Priority Programme “VGIscience”. The projects should focus on fundamental research and also clarify the relevance of their research questions and approaches with respect to a specific application domain.

Besides applications from individuals, also interdisciplinary tandem proposals are encouraged.

Proposals must be written in English and submitted to the DFG by **21 November 2018**. Please note that proposals can only be submitted via elan, the DFG’s electronic proposal processing system.

Applicants must be registered in elan prior to submitting a proposal to the DFG. You will normally receive confirmation of your registration by the next working day. Note that you will be asked to select the appropriate Priority Programme call during both the registration and the proposal process.

If you would like to submit a proposal for a new project within the existing Priority Programme, please go to Proposal Submission – New Project – Priority Programmes and select “SPP 1894” from the current list of calls. Previous applicants can submit a proposal for the renewal of an existing project under Proposal Submission – Proposal Overview/Renewal Proposal.

In preparing your proposal, please review the programme guidelines (Form 50.05, section B) and follow the proposal preparation instructions (form 54.01). These forms can either be downloaded from our website or accessed through the elan portal. In addition to submitting your proposal via elan, please send an electronic copy to the programme coordinator.


### Further Information

* [The DFG’s electronic portal “elan”][elan]
* DFG forms [50.05] and [54.01]
* [Call amendment][amendment]
* [Call in german][callger]

For scientific enquiries concerning the scope of the programme, please contact the Priority Programme’s coordinator:

Prof. Dr.-Ing. habil Dirk Burghardt  
Institute of Cartography, TU Dresden  
Helmholzstr. 10  
D-01062 Dresden, Deutschland  
Tel.: +49-351-463-36200  
dirk.burghardt@tu-dresden.de

Further instructions on submitting a proposal are supplied by the DFG:

Dr. Iris Sonntag  
Deutsche Forschungsgemeinschaft (DFG)  
-Physik, Mathematik, Geowissenschaften-  
D-53170 Bonn  
Tel. +49 (228) 885-2253  
iris.sonntag@dfg.de

[elan]:  https://elan.dfg.de
[50.05]: https://www.dfg.de/formulare/50_05
[54.01]: https://www.dfg.de/formulare/54_01
[amendment]: /images/2018-07-05-call-for-proposals-Anlage_zum_Aufforderungsschreiben_SPP_VGI.pdf
[callger]: /images/2018-07-05-call-for-proposals-SPP_1894_-_SPP-Aufforderung_Antragstellung.pdf
