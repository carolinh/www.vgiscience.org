---
layout: page
---
Enabling enhancement of scientific environmental data by volunteered geographic information: Extraction and visual assessment of data from social media images (ENAP)
================================================================================

**Professor Dr. Doris Dransch**  
*Helmholtz-Zentrum Potsdam, Deutsches GeoForschungsZentrum (GFZ), Department 1, Section 1.5: Geoinformatics, Telegrafenberg, 14473 Potsdam*

**Professor Dr.-Ing. Joachim Denzler**  
*Friedrich-Schiller-Universität Jena, Fakultät für Mathematik und Informatik, Institut für Informatik, Ernst-Abbe-Platz 2, 07743 Jena*


## Software and Data

- [European Flood 2013 Dataset](https://gitlab.vgiscience.de/ENAP/european-floods-2013-dataset) is a collection of 3,400 images with metadata and annotations about the European Flood 2013. Each image has been annotated with respect to its informativeness regarding inundation depth, extent, and water pollution. Moreover, particularly important regions have been highlighted on some iamges.
- [User Reputation](https://gitlab.vgiscience.de/uni-jena-inf/user-reputation) has data and analyses of the [NYPL Building Inspector](http://buildinginspector.nypl.org/) building footprint data set. Joint work with the [AGUI](algorithmically-guided-user-interaction-smart-crowdsourcing-and-the-extraction-of-metadata-from-old-maps.html) project.


## Original Proposal

The project‘s objective is to enable creation of data composites combining data from sensors and social media images for scientific purpose. Data composites are applied in environmental science for instance as input values for models or for validation of simulation model output. To fill missing values in data composites scientists use proxy-data that are derived from other related information. For instance, to fill missing values in climate data scientists derive proxy-data from ice cores. Proxy data has varying accuracy; therefore, they have to be assessed seriously, if they are appropriate for scientific use. We aim at developing a semi-automatic process chain that enables derivation of proxy-data from social media images and assessment of proxy-data with respect to their scientific value for a data composite. Our concrete use case is quantification of flood damage; here the data composites are used as input for flood damage models. The research topics we address are: a) filtering relevant images from social media streams, b) derive proxy-data from filtered images, c) assess proxy-data with respect to its scientific value for the data composite. We investigate these topics in a combined approach of computer vision and visual analytics. Results will be: i) enhanced computer vision methods to filter relevant images from social media streams and to extract determined proxy-data from filtered images, ii) a novel visual analytics approach to assess scientific value proxy-data can contribute to data composite regarding accuracy, coverage and resolution.
