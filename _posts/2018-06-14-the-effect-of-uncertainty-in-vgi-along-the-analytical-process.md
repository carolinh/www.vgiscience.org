---
layout: post
title: "The effect of Uncertainty in VGI along the analytical process"
date: "2018-06-14 14:23:23 +0200"
author:
    - Alexandra Diehl (Universität Konstanz)
---

![](/images/2018-06-14-the-effect-of-uncertainty-in-vgi-along-the-analytical-process.png)

There are various definitions of uncertainty in the literature with different level of agreement approached by different disciplines. Wordnet defines "uncertainty" as a state of being unsure about anything \[5\]. That said, uncertainty is an inevitable property of our daily life -- both at an abstract level for example, in our thinking and decision making process as well as at a measurement level. In general, uncertainty can be associated with concepts such as accuracy, error, quality, and precision, among others. Our research work explores the uncertainty in the information that are contributed by the volunteers with a specific focus on geographical aspects, which is termed as volunteered geographic information (VGI) \[4\]. Recently, with the emergence of quality assurance and soft computation there are some efforts to understand the uncertainty in VGI from the perspective of the contributors who create VGI \[4\] data and the analysts who analyze the data \[1\]. We consider the analysts and stakeholders as the consumers of VGI and the people who create the VGI as the producers \[2\].

VGI presents inherent subjectivity and fuzziness from the moment the information is captured, and these problems should be considered by the producer and also by the consumer. To analyze the effect of uncertainty along the analytical process, we introduce the term \"soft conceptualization\". The concept of soft conceptualization refers to the bias and knowledge the user built into the VGI information, based but departed from the definition of Chen et al. \[3\] about "soft knowledge". From this point of view, the effect of uncertainty rises some challenging questions such as: 

1. In which ways **are soft conceptualizations part of VGI** input data?
2. How can we identify, extract, and measure the effect of those soft conceptualizations in the underlying uncertainty?
3. Beyond a nuisance viewpoint: how can we **utilize soft     conceptualizations** to reduce the uncertainty, to discriminate what is certain for a specific domain, user, task, and VGI?

**References**

\[1\] Sacha, Dominik, et al. \"The role of uncertainty, awareness, and trust in visual analytics.\" IEEE transactions on visualization and computer graphics 22.1 (2016): 240-249.

\[2\] Grira, J., Bédard, Y., & Roche, S. (2010). Spatial data uncertainty in the VGI world: Going from consumer to producer. Geomatica, 64(1), 61-72.

\[3\] Chen, M., Grinstein, G., Johnson, C. R., Kennedy, J., & Tory, M. (2017). Pathways for theoretical advances in visualization. IEEE computer graphics and applications, 37(4), 103-112.

\[4\] Goodchild, M. F. Citizens as Sensors: The World of Volunteered Geography. In M. Dodge, R. Kitchin, & C. Perkins (Eds.), The Map Reader: Theories of Mapping Practice and Cartographic Representation: John Wiley & Sons, Ltd.

\[5\] Miller, G. A. (1995). WordNet: A Lexical Database for English. Communications of the ACM, 38(11), 39-41.
