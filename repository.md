---
layout: page
title: VGIscience Collaboration Repository
menu: Repository
order: 6
---


![VGIscience tech modules](/assets/images/tech-modules.svg)

**Open and Reproducible Science**

On [repository.vgiscience.org][1] we run a repository platform that provides the basis for our collaboration within the priority program. The platform itself is based on the free and open source software [GitLab][2], a versatile tool dedicated to support software development and collaboration. We extend the usage of this tool for our own purposes:

* share, organize, debug and run code created within the priority program
  * e.g. [TagMaps][7], [glyph-miner][8], [lineman][15], [ClipGeo][16], [User Reputation Model][19], [LBSN Datastructure Concept][20]

* share knowledge and other material
  * e.g. [research data][9], [best practices][10]

* create and manage content for the VGIscience website (this site) and related project sites
  * e.g the [summer school][3], the [HCRW][4]

* create and share templates for presentation slides and web pages
  * e.g. [VGIscience website template][10], [VGIscience presentation template][17]

* create, build, publish and share scientific papers in standardized templates
  * e.g. [IEEE PDF][11], [Word doc][18]

* organize members in groups for their home universities and research projects within the priority program

* Single-Sign-On Provider for further services that are important for our internal collaboration
  * [team collaboration chat service][5]
  * [real-time collaboration notepad][6]


<style>
.post-content > ul > li > ul > li {
    text-indent: 0;
    margin-left: -1em;
}
</style>


[1]: https://repository.vgiscience.org/explore/projects/starred
[2]: https://about.gitlab.com
[3]: https://summerschool.vgiscience.org
[4]: https://hcrw.vgiscience.org
[5]: https://chat.vgiscience.org
[6]: https://notepad.vgiscience.org
[7]: https://gitlab.vgiscience.de/ad/tagmaps
[8]: https://gitlab.vgiscience.de/thomas.vandijk/glyph-miner
[9]: https://gitlab.vgiscience.de/KeilJu/osm-landmark-pictogram-review
[10]: https://gitlab.vgiscience.de/templates/website
[11]: https://gitlab.vgiscience.de/templates/papers
[12]: https://gitlab.vgiscience.de/www/www.vgiscience.org
[13]: https://gitlab.vgiscience.de/summerschool/summerschool.vgiscience.org
[14]: https://gitlab.vgiscience.de/hcrw/hcrw.vgiscience.org
[15]: https://gitlab.vgiscience.de/thomas.vandijk/lineman
[16]: https://gitlab.vgiscience.de/ad/ClipGeo
[17]: https://gitlab.vgiscience.de/templates/presentation
[18]: https://gitlab.vgiscience.de/templates/word-template
[19]: https://gitlab.vgiscience.de/uni-jena-inf/user-reputation
[20]: https://gitlab.vgiscience.de/lbsn/concept
