---
layout: page
---
Extraction and visually driven analysis of geography and dynamics of people's reaction to events (EVA-VGI)
================================================================================


**Professor Dr.-Ing. habil. Dirk Burghardt**  
*Technische Universität Dresden, Institut für Kartographie, 01062 Dresden*

**Professor Dr. Stefan Wrobel**  
*Rheinische Friedrich-Wilhelms-Universität Bonn, Institut für Informatik III, Römerstraße 164, 53117 Bonn | Director of Fraunhofer Institute IAIS*

**Professor Dr. Ross Purves**  
*Universität Zürich, Geographisches Institut*

In the first phase of the project, we developed and demonstrated a conceptual model enabling the extraction, analysis and visualisation of events and reactions to events in location based social media (LBSM). A central element of this model and its implementation is the integration of spatial, temporal, thematic and social dimensions combined with an explicit link between events and reactions.

## Notable outcome

* [LBSN Datastructure Concept], a common language independent and cross-network social-media datascheme 
* [in our news on  May 25, 2018]
* [Python compiled version of ProtoBuf] spec ([pypi-ref][2])
* [LBSNTransform], import, transform and export Social Media data such as Twitter and Flickr ([pypi-ref][3])

## Selected publications

<ul class="publications">
    <li class="article-journal">Hauthal, E., Burghardt, D., &amp; Dunkel, A. (2019). Analyzing and Visualizing Emotional Reactions Expressed by Emojis in Location-Based Social Media. <i>International Journal of Geo-Information</i>, <i>8</i>(3), 21. https://doi.org/10.3390/ijgi8030113 <span class="opt">[DOI:<a href="https://doi.org/10.3390/ijgi8030113">10.3390/ijgi8030113</a> | <a href="https://www.bibsonomy.org/bib/publication/107e856e07ff016fe858a5b6290ce739/siebold">BibTeX</a> | <a href="https://www.bibsonomy.org/publication/107e856e07ff016fe858a5b6290ce739/siebold">BibSonomy</a>]</span></li>
    <li class="paper-conference">Das, R. D., &amp; Purves, R. S. (2018). Towards the Usefulness of User-Generated Content to Understand Traffic Events (Short Paper). In <i>10th International Conference on Geographic Information Science (GIScience 2018)</i>. Schloss Dagstuhl-Leibniz-Zentrum fuer Informatik. <span class="opt">[<a href="https://www.bibsonomy.org/bib/publication/738e4b4c185db28c6fec2adf4ec058e9/vgscnc">BibTeX</a> | <a href="https://www.bibsonomy.org/publication/738e4b4c185db28c6fec2adf4ec058e9/vgscnc">BibSonomy</a>]</span></li>
    <li class="article-journal">Chen, S., Li, J., Andrienko, G., Andrienko, N., Wang, Y., Nguyen, P. H., &amp; Turkay, C. (2018). Supporting Story Synthesis: Bridging the Gap between Visual Analytics and Storytelling. <i>IEEE Transactions on Visualization and Computer Graphics</i>. <span class="opt">[<a href="https://www.bibsonomy.org/bib/publication/4193ecbb38770dd1b058153437af5c77/vgscnc">BibTeX</a> | <a href="https://www.bibsonomy.org/publication/4193ecbb38770dd1b058153437af5c77/vgscnc">BibSonomy</a>]</span></li>
    <li class="article-journal">Chesnokova, O., &amp; Purves, R. S. (2018). From image descriptions to perceived sounds and sources in landscape: Analyzing aural experience through text. <i>Applied Geography</i>, <i>93</i>, 103–111. <span class="opt">[<a href="https://www.bibsonomy.org/bib/publication/6209af7246de51e2f8e2bd0f31d869dc/vgscnc">BibTeX</a> | <a href="https://www.bibsonomy.org/publication/6209af7246de51e2f8e2bd0f31d869dc/vgscnc">BibSonomy</a>]</span></li>
    <li class="article-journal">Dunkel, A., Andrienko, G., Andrienko, N., Burghardt, D., Hauthal, E., &amp; Purves, R. (2018). A conceptual framework for studying collective reactions to events in location-based social media. <i>International Journal of Geographical Information Science</i>, 1–25. https://doi.org/10.1080/13658816.2018.1546390 <span class="opt">[DOI:<a href="https://doi.org/10.1080/13658816.2018.1546390">10.1080/13658816.2018.1546390</a> | <a href="https://www.tandfonline.com/doi/full/10.1080/13658816.2018.1546390">URL</a> | <a href="https://www.bibsonomy.org/bib/publication/b64657db58afe96f1e53eb6a0af37d2b/siebold">BibTeX</a> | <a href="https://www.bibsonomy.org/publication/b64657db58afe96f1e53eb6a0af37d2b/siebold">BibSonomy</a>]</span></li>
</ul>

## Original proposal

The rise of so-called Volunteered Geographic Information (VGI) has brought with it fundamental changes not only in the nature of geographic data, but also its production and accessibility. These changes have implications across the board for those carrying out research requiring geographic data, and most profoundly for research exploring how humans interact with and are affected by changes in their environment. In this project we propose moving beyond event detection, to investigate ways in which events can be characterised according to four key dimensions: spatial, temporal, thematic and social context. This characterisation will allow us to capture information about how groups of users are both affected by, and react to events and their evolution. To do so will require us to relate a variety of data sources with heterogeneous properties, which in some cases are generated with high velocities and volumes and have multiple granularities.

Our approach to characterising events has the following central pillars: data extraction and event identification; analysis of data content and semantics and associated interactive, dynamic visualisation. Analysis methods typical generate more information, and as such, given the large volumes of data that are our starting point will require effective visual analytics approaches enabling users to interact with the expected large data volumes. Our objective is therefore to develop a system of integrated visual and computational methods enabling investigation of people’s reactions to significant events through analysis of volunteered geographic information containing both explicit and implicit georeferences. Thus we will firstly develop a taxonomy of events focusing on the four mentioned dimensions associated with events and ways in which events are reacted to. The extraction of events from VGI will be based on a combination of spatial, temporal and textual analysis methods to link events, groups and their characteristics to space, time and social context. Our analysis will focus on reactions to events, and in so doing the socio-demographics of involved groups. In the analysis of the social aspects of group reactions to events a central tenet will be the use of privacy preserving methods which effectively obfuscate individuals.

We will illustrate our concepts through the implementation of a set of demonstrators including (but not limited to) disaster management, public involvement in local affairs, migration, climate and environment, social geography and medicine and health. To achieve our aims we have brought together a project team with expertise in investigating VGI, for example exploring the spatio-temporal behaviour from such data, describing the semantics of specific and generic aspects of place, and analysing affective data associated with location.



[LBSN Datastructure Concept]: https://gitlab.vgiscience.de/lbsn/concept
[In our news on  May 25, 2018]: https://www.vgiscience.org/2018/05/25/heidelberg-a-common-data-structure-concept.html
[Python compiled version of ProtoBuf]: https://gitlab.vgiscience.de/lbsn/lbsnstructure-python
[2]: https://pypi.org/project/lbsnstructure
[LBSNTransform]: https://gitlab.vgiscience.de/lbsn/lbsntransform
[3]: https://pypi.org/project/lbsntransform
