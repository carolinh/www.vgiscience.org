---
layout: page
---
Motivation and Participation of Digital Volunteer Communities in Humanitarian Assistance: Models and Incentives for Closing the Gap to Decision Makers
================================================================================


**Professor Dr.-Ing. Frank Fiedrich**  
*Bergische Universität Wuppertal, Fakultät für Maschinenbau und Sicherheitstechnik, Lehrstuhl für Bevölkerungsschutz, Katastrophenhilfe und Objektsicherheit, Lise-Meitner-Straße 11-13, 42119 Wuppertal*

In the first project phase, we analysed the motivational factors, participation barriers and incentives for digital volunteers in Volunteer & Technical Communities (V&TC). 
We developed structure and process requirements which are necessary for an effective collabroation in time-critical decision-making processes in emergency and disaster management. 

## Selected publications

* Fathi, R., Thom, D., Koch, S., Ertl, T., & Fiedrich, F. (2020). VOST: A case study in voluntary digital participation for collaborative emergency management. Information Processing & Management, 57(1). [https://doi.org/10.1016/j.ipm.2019.102174](https://doi.org/10.1016/j.ipm.2019.102174)
* Fiedrich, F. & Fathi, R. (2018). Humanitäre Hilfe und Konzepte der digitalen Hilfeleistung. Sicherheitskritische Mensch-Computer-Interaktion: Interaktive Technologien und Soziale Medien im Krisen- und Sicherheitsmanagement, pp. 509-528. Springer Vieweg. [https://doi.org/10.1007/978-3-658-19523-6_25](https://doi.org/10.1007/978-3-658-19523-6_25)
* Fathi R., Brixy AM., Fiedrich F. (2019) Desinformationen und Fake-News in der Lage: Virtual Operations Support Team (VOST) und Digital Volunteers im Einsatz. In: Lange HJ., Wendekamm M. (eds) Postfaktische Sicherheitspolitik. Studien zur Inneren Sicherheit, vol 23. Springer VS, Wiesbaden. [https://doi.org/10.1007/978-3-658-27281-4_11](https://doi.org/10.1007/978-3-658-27281-4_11)
* Fathi, R., Fiedrich, F. (2017). Motivation and Participation of Digital Volunteer Communities in Humanitarian Assistance. Conference: Proceedings of the 14th International Conference on Information Systems for Crisis Response And Management (pp. 412-419). Albi, France. ISSN: 2411-3387
* Fathi, R., Polan, F., Fiedrich, F. (2017). Digitale Hilfeleistung und das Digital Humanitarian Network. Notfallvorsorge, 3/2017, pp. 4-10. Walhalla-Verlag. ISBN: 978-3-8029-4875-6 

## Notable outcome

* Implementation and exploration of a new form of collaborative digital volunteering effort, called [Virtual Operations Support Team (VOST)](https://www.vgiscience.org/2018/10/07/virtual-operations-support-teams.html) in cooperation with [VA4VGI](https://www.vgiscience.org/projects/va4vgi.html)
* Participating observation of decision-making processes of emergency operation center (EOC) using VGI in real and time-critical operations (see [cooperation paper](https://www.sciencedirect.com/science/article/abs/pii/S0306457319302316))  
* Analysis of digital volunteers from international V&TCs in disaster relief (e.g. image)
![](../images/Frame_Communities.jpg) 


## Original proposal

Since a number of years, Volunteered Geographic Information (VGI) plays an important role in the management of large scale disasters and in Humanitarian Assistance. Digital volunteer communities such as the CrisisMapper community, the Standby Task Force, the UAViators or Virtual Operations Support Teams (VOST) have a high potential to improve the decision making processes by providing adequate data which otherwise would not be available. While a lot of research focuses on the extraction, validation and visualization of VGI data the motivational aspects of these communities are hardly understood. A better understanding of motivational and impeding factors would help to identify critical success factors and incentives in order to increase not only the number of volunteers but also the quality of the provided data. The proposed research project will provide a categorisation scheme for different volunteer groups based on their degree of organizational structure and the degree of the individual linkage of the volunteers to the respective community. We will analyse motivational success factors for selected volunteer groups with a mixed method approach including surveys, interviews and focus groups. On the other side, the information needs of the humanitarian response groups will be analysed and compared to the potential tasks of the volunteer communities. We assume that a closer link between volunteers and responders will not only increase the quality of the provided data but also the motivation of the volunteers and therefore help to close the current gap and increase the collaboration between volunteers and responders. The proposed research will also analyse passive digital volunteers like Twitter users. This group is often not aware that they provide valuable VGI data. Based on the content analysis of Twitter data from past events different categories of this specific volunteer group will be identified. Comparisons to the results from the motivational study and semistructured interviews will be used to develop a model for the activation of passive VGI contributors. The results of the proposed research will also provide valuable links to other research aspects of the SPP 1894 priority programme like big data analysis, visualization and quality assurance.
