---
layout: page
---
[IDEAL-VGI](https://www.geog.uni-heidelberg.de/gis/ideal_en.html): Information Discovery from Big Earth Observation Data Archives by Learning from Volunteered Geographic Information 
================================================================================


**[Prof. Dr. Begüm Demir](https://www.rsim.tu-berlin.de/menue/team/prof_dr_beguem_demir/parameter/en/)**  
*Technische Universität Berlin, Department of Computer Engineering and Microelectronics, Remote Sensing Image Analysis (RSiM) Group*

**[Prof. Dr. Alexander Zipf](https://www.geog.uni-heidelberg.de/gis/zipf_en.html)**  
*Heidelberg University, Institute of Geography, GIScience / Geoinformatics Research Group*

**[Dr. Sven Lautenbach](https://www.geog.uni-heidelberg.de/gis/lautenbach.html)**  
*Heidelberg University, Institute of Geography, GIScience / Geoinformatics Research Group*

**[Dr. Michael Schultz](https://www.geog.uni-heidelberg.de/gis/schultz.html)**  
*Heidelberg University, Institute of Geography, GIScience / Geoinformatics Research Group*

**[M.Sc. Tristan Kreuziger](https://www.rsim.tu-berlin.de/menue/team/tristan_kreuziger/parameter/en/)**  
*Technische Universität Berlin, Department of Computer Engineering and Microelectronics, Remote Sensing Image Analysis (RSiM) Group*

**[M.Sc. Moritz Schott](https://www.geog.uni-heidelberg.de/gis/schott.html)**  
*Heidelberg University, Institute of Geography, GIScience / Geoinformatics Research Group*

During the last decade, huge amounts of remote sensing (RS) images have been acquired, leading to massive Earth Observation (EO) data archives from which mining and retrieving useful information are challenging. Volunteered Geographic Information (VGI) such as OpenStreetMap (OSM) can offer rich geometric and semantic information that goes beyond land use tags, which can be very beneficial for accessing and extracting vital information for observing Earth from big Earth Observation archives. However, user-provided tags within OSM can be noisy, incomplete and redundant.

The [IDEAL-VGI](https://www.geog.uni-heidelberg.de/gis/ideal_en.html) project aims to address very important scientific and practical problems by focusing on the main challenges of:

  1. VGI for land use classification which are: a missing framework to exploit the rich semantic information present at different scales and the uncertainty of OSM derived land use classes.
  2. Big EO data, which are: RS image characterization, indexing and search from massive archives.

To this end, we will develop innovative methods, which can significantly improve the state-of-the-art both in the theory and in the tools currently available. In particular, novel methods will be developed, aiming to:

  1. identification of the importance, uncertainty and quality of different OSM derived features;
  2. enhancing methods for better assessment of quality to promote relevant semantic content of OSM and integration of supporting complementary VGI data streams;
  3. developing machine learning/deep learning algorithms in the framework of RS image classification for automatic OSM tag refinement and assignment;
  4. developing RS image classification, search and retrieval methods that consider OSM tags with their uncertainty information;
  5. improve both OSM semantic land use description as well as remote sensing image classification based on a comparison between the two classification approaches;
  6. make full use of VGI to generate accurate annotated data sets and improve accuracy of labelling, which should contribute to more convincing training data sets.