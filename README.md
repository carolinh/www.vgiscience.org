# Welcome to the VGIscience Website Project

This repository contains the content of the [VGIscience website](https://www.vgiscience.org).

## How to create blog posts

Create news or blog posts by adding files to the `_posts` directory. Use the format `yyyy-mm-dd-title-of-post.md` without spaces, caps or umlauts for the file name.

Start the file with the following YAML-frontmatter:

    ---
    layout: post
    title: "The title of your Post"
    date: 2018-02-12 12:14
    author:
        - First Name Last Name (Department, University)
        - More authors, if any
    ---

When you hit the green `Commit changes` button, the website is being rebuilt. It takes a moment for the build process to finish. You can watch it on the jobs page (*CI/CD* -> *Jobs* in the menu on the left).

## Bibsonomy

If you have [added new publications to Bibsonomy](https://gitlab.vgiscience.de/general/knowledge/wikis/bibsonomy-publications), this repository needs to be rebuilt to list them on the [publications page](https://www.vgiscience.org/publications.html). You can trigger the build yourself, if you are a member of the [website group](https://gitlab.vgiscience.de/groups/www). Just go to [the CI/CD Jobs page of the Website project](https://gitlab.vgiscience.de/www/www.vgiscience.org/-/jobs) and hit the `⭮` button of the latest passed job.
