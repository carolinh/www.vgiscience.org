---
layout: page
title: Projects
order: 3
---

[Geovisual analysis of VGI for understanding people's behaviour in relation to multi-faceted context (EVA-VGI 2)](projects/eva-vgi-2.html)  
Prof. Dirk Burghardt, Dresden | Prof. Stefan Wrobel, Bonn | Prof. Ross Purves, Zürich

[WorldKG: World-Scale Completion of Geographic Knowledge](projects/worldkg.html)  
Dr. Elena Demidova, L3S Research Center, Hannover

[Visual analysis of volunteered geographic information for interactive situation modeling and real-time event assessment (VA4VGI 2)](projects/va4vgi-2.html)  
Professor Dr. Thomas Ertl, Stuttgart | Dr. Steffen Koch, Stuttgart

[Information Discovery from Big Earth Observation Data Archives by Learning from Volunteered Geographic Information (IDEAL-VGI)](projects/IDEAL-VGI.html)  
Prof. Dr. Begüm Demir, Berlin | Prof. Dr. Alexander Zipf, Heidelberg | Dr. Sven Lautenbach, Heidelberg | Dr. Michael Schultz, Heidelberg | M.Sc. Tristan Kreuziger, Berlin | M.Sc. Moritz Schott, Heidelberg

[Inferring Personalized Multi-criteria Routing Models from Sparse Sets of Voluntarily Contributed Trajectories](projects/vgi-routing.html)  
Professor Dr.-Ing. Jan-Henrik Haunert, Bonn | M. Sc. Axel Forsch, Bonn

[Uncertainty-Aware Enrichment of Animal Movement Trajectories by VGI](projects/animal-trajectories.html)  
Professor Dr. Daniel Keim, Konstanz | M.Sc. Philipp Meschenmoser, Konstanz

[ExpoAware – Environmental volunteered geographic information for personal exposure awareness and healthy mobility behaviour](projects/expoaware.html)  
Prof. Dr. Uwe Schlink, Leipzig

# Projects involved in the first phase of the priority programme

[Extraction and visually driven analysis of geography and dynamics of people's reaction to events (EVA-VGI)](projects/eva-vgi.html)  
Prof. Dirk Burghardt, Dresden | Prof. Stefan Wrobel, Bonn | Prof. Ross Purves, Zürich

[The Effects of Landmarks on Navigation Performance in VGI-based Maps](projects/landmarksvgi.html)  
Prof. Frank Dickmann, Bochum | Prof. Lars Kuchinke, Bochum

[Algorithmically-Guided User Interaction: Smart Crowdsourcing and the Extraction of Metadata from Old Maps](projects/old-maps.html)  
Dr. Thomas van Dijk, Würzburg | Prof. Alexander Wolf, Würzburg

[Enabling enhancement of scientific environmental data by volunteered geographic information: Extraction and visual assessment of data from social media images (ENAP)](projects/enap.html)  
Prof. Doris Dransch, Potsdam | Prof. Joachim Denzler, Jena

[Visual analysis of volunteered geographic information for interactive situation modeling and real-time event assessment (VA4VGI)](projects/va4vgi.html)  
Prof. Thomas Ertl, Stuttgart | Dr. Steffen Koch, Stuttgart | Dr. Dennis Thom, Stuttgart

[A framework for measuring the fitness for purpose of OpenStreetMap data based on intrinsic quality indicators](projects/qualityosm.html)  
Dr. Hongchao Fan, Heidelberg

[Motivation and Participation of Digital Volunteer Communities in Humanitarian Assistance: Models and Incentives for Closing the Gap to Decision Makers](projects/dvcha.html)  
Prof. Frank Fiedrich, Wuppertal

[Human-Centered Relational Feature Classification for VGI](projects/hcrfc.html)  
Prof. Christian Freksa, Bremen

[Lightweight Acquisition and Large-scale Mining of Trajectory Data](projects/trajectory-data.html)  
Prof. Stefan Funke, Stuttgart | Dr. Sabine Storandt, Würzburg

[P2MAP: Learning Environmental Maps - Integrating Participatory Sensing and Human Perception](projects/p2map.html)  
Prof. Andreas Hotho, Würzburg

[Guiding the Crowds: Uncertainty and Trust-Aware Visual Analytics for Mobility based on Heterogeneous Volunteered Geographic Information](projects/uncertainty.html)  
Prof. Daniel Keim, Konstanz

[topikos - Collaborative Low-Effort Topological and Topical-Social Indoor Mapping](projects/topikos.html)  
Prof. Gerd Stumme, Kassel

[Context-Sensitive Qualitative Spatial Reasoning for Interpreting Vague Place Descriptions](projects/spatial-reasoning.html)  
Prof. Diedrich Wolter, Bamberg

[COVMAP: Comprehensive Conjoint GPS and Video Data Analysis for Smart Maps](projects/covmap.html)  
Prof. Carsten Rother, Heidelberg | Prof. Michael Ying Yang, Twente | Prof. Bodo Rosenhahn, Hannover

[Spatial Correlations in Social Media Data: Identification and Quantification of Spatial Correlation Structures in Georeferenced Twitter Feeds](projects/spatial-correlations.html)  
Prof. Alexander Zipf, Heidelberg
