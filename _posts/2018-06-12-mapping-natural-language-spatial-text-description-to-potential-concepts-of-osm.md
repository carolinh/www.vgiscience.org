---
layout: post
title: "Mapping Natural Language Spatial Text Description to Potential Concepts of OSM"
date: "2018-06-12 15:29:47 +0200"
author:
    - Madiha Yousaf (University of Bamberg)
    - Daniel Bahrdt (University of Stuttgart)
---

The Basic idea is to capture context-sensitive vague spatial information in human-generated vague place descriptions and 
to develop algorithms that allow geographic databases to be queried using such descriptions.

For this purpose the database we are using is OSM , to resolve ambiguities and vagueness present inside the textual descriptions in this project we will be 
querying OSM search engine Pyoscar (a library in python developed by our collaboration partners in stuttgart ) using its 
functions for search, containment and intersection,providing it with a natual language processing interface and trying to query it by implementing the correct 
semantics of spatial realtions (major task)required to solve ambiguites present in the text. 

We demonstrate our appraoch using the spatial text descriptions from wikipedia (any other text descriptio source can be used) where, if the user enters train station in bamberg 
then the text description entered should be mapped to OSM query syntax and proving us with the identification of Bamberg,train station and the correct semantics 
of in(containment) relations. 

Future plan includes to improve correct semantics for all possible topological relations present in  for querying OSM and getting desired(correct) answers.
