---
layout: page
---
Cognitive Effects of Landmarks in VGI-based Maps
====================================================================

**Professor Dr. Frank Dickmann**  
*Ruhr-Universität Bochum, Geographisches Institut, Arbeitsgruppe Geomatik, Universitätsstraße 150, 44801 Bochum*

**Professor Dr. Lars Kuchinke**  
*Ruhr-Universität Bochum, Fakultät für Psychologie, Arbeitsgruppe Experimentelle, Psychologie und Methodenlehre, Universitätsstraße 150, 44801 Bochum*


In the era of GPS capable smartphones and mobile internet, maps are no longer solely created by commercial companies or public institutions. Similar to other open content initiatives like Wikipedia or Freesound, web-mapping services have been established that rely mostly on (geographic) data gathered and submitted by volunteers. The most popular of these collaborative web-mapping services is OpenStreetMap (OSM). Its dependency on volunteered geographic information (VGI) leads to substantial design and content differences when compared to commercial or administrative maps (Cipeluch, Jacob, Mooney, & Winstanley, 2010). 

In contrast to centralized map providers, design characteristics of OSM are not based on corporate or administrative decisions, but on suggestions and discussions of contributors. Contributors can also develop and publish own design templates. Concerning content, the amount of local contributors and their personal preferences affect the completeness of map information and the order in which geographic elements are localized (Cipeluch et al., 2010; Haklay, 2010). Whereas map representations of regions with only few contributors may be relatively incomplete, the completeness of map representations can exceed that of non-VGI-based maps in specific regions when many or strongly motivated contributors are located in these regions. Taken together, VGI-based mapping services are less consistent than commercial or administrative maps. To investigate how this inconsistency of map information may affect map use and related spatial tasks as orientation, navigation and the formation of mental representations of space (also called cognitive maps), it is necessary to understand how people interact with map elements in general. 

One type of map elements that plays an important role in map perception and use are landmark representations. Landmarks are salient and memorable objects with a fixed geographic location (Anacta, Schwering, Li, & Muenzer, 2017; Sorrows & Hirtle, 1999). In maps, landmarks are represented by pictograms. These landmark representations are important elements for orientation, navigation, and the formation of cognitive maps (Elias & Paelke, 2008; Foo, Warren, Duchon, & Tarr, 2005; Golledge, 1999; Millonig & Schechtner, 2007). Thus, findings about how landmark characteristics like pictogram design, position, and salience affect their perception and use will likely further our knowledge on how orientation, navigation and the formation of cognitive maps is processed. Additionally, in regions with less OSM contributors, such findings could be used as guidelines to decide which objects should be localized with a higher priority, or which landmark pictograms should be replaced or modified. In the DFG (SPP 1892) project “the effects of landmarks on navigation performance in VGI-based maps”, we experimentally investigate and quantify effects of landmark pictogram design, position and salience to provide such guidelines.

In a first step, we investigated the effects of OSM landmark pictogram design on their salience, meaningfulness, and recognition performance (Keil, Edler, Dickmann & Kuchinke, 2018b; 2019a). The motivation for this investigation was that the design of OSM landmark pictograms is not based on an iterative process accompanied by usability tests. Therefore, we could not rule out that some pictograms attract less visual attention (visual salience) or that some are more difficult to interpret (meaningfulness) or to memorize than other pictograms. This would impair the usefulness of these pictograms, as they may be overlooked, misinterpreted, or forgotten. As anticipated, in an experiment using eye tracking to assess the visual salience and self-reports to assess the meaningfulness of 153 OSM landmark pictograms, we were able to show that both visual salience and meaningfulness varied strongly (see Fig. 1, left chart). Using a recognition task, we also assessed the memorability of pictograms. Subsequently, we investigated potential relations between the three measures. 

Correlation analyses showed that meaningful pictograms were less salient and less memorable (see Fig. 1). Attempts to understand pictograms with a low meaningfulness might have shifted visual attention towards these pictograms. Consequentially, the higher visual attention could explain why they were recalled more accurately. Based on these conclusions, it could be argued that only pictograms with low meaningfulness should be used, as they were found to be more salient and memorable. However, this would also make it very difficult to understand what a pictogram represents and, consequentially, to link the representation to the represented landmark. Thus, the landmark representations would lose their purpose. Therefore, the findings should be used to optimize pictograms with low meaningfulness. It is expected that this would equalize the visual salience of all landmark pictograms and thereby direct visual attention to map elements that are relevant for tasks as orientation, navigation and the formation of cognitive maps.

![Relation between the meaningfulness of OSM landmark pictograms, visual salience and recognition performance](../images/landmarksFig1.png)

**Fig. 1 Relation between the meaningfulness of OSM landmark pictograms, visual salience and recognition performance.** *The left chart shows the relation between the meaningfulness of pictograms (measured with self-reports) and their visual salience (measured based on the total fixation duration). The right chart shows the relation between the meaningfulness of pictograms and their recognition performance (measured as d’). High values of d’ represent a better recognition performance (discrimination between old and new stimuli). Both the visual salience and the recognition performance was lower for pictograms with a high meaningfulness.*

The following project steps investigated which landmark representations are relevant for different map-based tasks based on their (relative) position in order to detect likely patterns of use. The first task to be investigated was route memory. With knowledge about the relation between the relative position of landmark representations and their relevance for memorizing a route, it would be possible to apply task-oriented reduction of map content to direct the visual attention of users to task-relevant map elements. Our assumption was that relevance of landmark representations is inverse to its distance to the to-be-learned route. Additionally, as mental representations of space are formed based on relations between single spatial elements (McNamara & Valiquette, 2004; Tversky, 2003), we expected that more distant landmark representations are task-relevant when less spatial elements are available in a map, e.g. in maps of rural areas (low visual complexity). Two route recall experiments using different maps and a variation of map scales and visual map complexity were carried out to test these assumptions (Keil, Edler, Kuchinke & Dickmann, 2019b; 2019c). As it has been argued that eye fixations represent cognitive processing of visual stimuli (Grant & Spivey, 2003; Just & Carpenter, 1976) and map elements require cognitive processing to be used in map-based tasks, eye fixations were recorded in both experiments and used as a measure of task-relevance. 

The findings of these experiments supported our assumptions. Correlations between eye fixations on landmark pictograms (fixation count, total fixation duration, mean fixation duration) and the distance of the pictograms to the route were all negative and highly significant in both experiments (all rs < -.8 all p <.001). In other words, the results indicate that cognitive processing was more pronounced for landmark pictograms close to the route. Therefore, we argue that landmark pictograms close to the route were more relevant for memorizing the route.

![Heat map of accumulative fixations on a map in a route memory task](../images/landmarksFig2.png)

**Fig. 2 Heat map of accumulative fixations on a map in a route memory task.** *When people tried to memorize the displayed route, most visual attention was directed towards map elements close to the route.*

We also found that when the amount of map elements per cm² is reduced by stretching a subsection of the map to a larger size, landmark pictograms further offside the route were fixated (see Fig. 3). We explain this result with the reduced number of spatial reference points available to memorize the route. People seem to require a certain number of spatial elements to memorize a route. If not enough spatial elements are available close to the route, they may use spatial elements further offside the route.

![Mean distance to the route of fixated landmark pictograms in pixels](../images/landmarksFig3.png)

**Fig. 3 Mean distance to the route of fixated landmark pictograms in pixels.** *When maps were stretched to reduce their visual complexity, landmarks further offside the route were fixated.*

The mentioned experiments demonstrate which map elements people prefer to use in route memory tasks. However, the findings do not demonstrate whether the perception and use of specific map elements actually affects route memory performance. To investigate which map elements are required to effectively memorize a route, we carried out another route memory experiment (Keil, Mocnik, Edler, Dickmann & Kuchinke, 2018a). In this experiment, fixations on landmark pictograms and route memory performance were compared between unmodified maps (standard map) and maps, which displayed areas more than 10 pixels offside the route transparently (reduced map). As transparency can direct visual attention (Colby & Scholl, 1991; Sutherland, McQuiggan, Ryan, & Mather, 2017), we used it with the intention to shift attention towards landmark pictograms close to the route. Thus, we expected that areas offside the route would be fixated less often when these areas were transparent. Additionally, if areas offside the route are, in fact, irrelevant for memorizing the route, we expected that memory performance should not differ between the two map conditions (reduced map / standard map). 

In agreement with our expectations, displaying areas offside the route transparently directed visual attention towards the route. Almost no fixations on the transparent map areas were measured (see Fig. 4). As expected, no statistically significant differences of route recall performance were found between the two map conditions (reduced map / standard map), even though participants visually processed areas offside the route significantly less when these areas were transparent. Therefore, we concluded that areas offside the route and the spatial elements in this area, as landmark pictograms, were not required to memorize the route. However, as indicated by the experiments described above, the width of the area around a route that is required to memorize the route may be affected by the visual complexity of the map.

![Fixations on the area close to the route and areas offside the route compared between the two map conditions (reduced map / standard map)](../images/landmarksFig4.png)

**Fig. 4 Fixations on the area close to the route and areas offside the route compared between the two map conditions (reduced map / standard map).** *When areas offside the route were displayed transparent, fixations counts on these areas were significantly lower than in the unmodified maps.*

The final phase of the ongoing research project will focus on studies concerning the relevance of landmark representations in object location memory tasks. As object locations are the building blocks for the formation of mental representations of space (McNamara & Valiquette, 2004; Tversky, 2003), it is important to understand which spatial elements people select to build a relational spatial model. This would allow to selectively display task-relevant map elements as landmarks, and to remove irrelevant map elements or to reduce their visibility. Similar to the route memory experiments, the relation between visual attention and the distance of landmark pictograms to the to-be-learned object locations will be investigated, as we expect closer landmarks to be more relevant. Eye movements will be recorded to investigate the relation between the mean distance of fixated landmarks and object location recall performance. This will allow us to test whether the use of closer landmarks as reference points will, as expected, lead to more accurate object location recall.

Additionally, the position of landmark pictograms relative to the (imaginary) x- and y-axes of the map will be investigated as another predictor for object location memory performance. This is based on the assumption that people may use Gestalt principles as parallelism to build mental representations of space. As maps usually have a rectangular shape, the parallelism of the x- and y-axes to the map borders could act as an additional information layer to structure the map content. Related effects of an illusory grid information layer on object location memory have been reported by Dickmann, Edler, Bestgen, and Kuchinke (2016). They argued that people use the Gestalt principle of closure to connect grid fragments into a complete structure. Based on these findings, it can be assumed that people can also use parallelism to memorize object locations relative to the position of landmark pictograms. Therefore, we expect that the availability of landmarks close to the x- or y- axis of the to-be-learned object location could improve object location memory performance. 

Summary
--------

The above-mentioned studies concerning the use of landmark representations in map-based tasks provide first insights into how people perceive and use landmark representations. Especially in VGI-based maps with unevenly distributed contributions of spatial data, the described findings can be used to develop guidelines for contributors to localize seemingly less relevant landmarks in poorly mapped areas. The use and display of areas mapped in high detail on the other hand might also benefit from the findings. One of the project goals was to investigate which map elements are irrelevant in specific tasks. The identified predictors for the relevance of landmark pictograms for route memory could be used to reduce the amount of map elements and to direct visual attention towards relevant map elements. The described last project phase is meant to extend similar suggestions to location memory tasks. 

Project Publications
--------

Bestgen, A.-K., Edler, D., Kuchinke, L., & Dickmann, F. (2017). Analyzing the Effects of VGI-based Landmarks on Spatial Memory and Navigation Performance. KI - Künstliche Intelligenz, 31(2), 179–183. https://doi.org/10.1007/s13218-016-0452-x

Keil, J., Mocnik, F.-B., Edler, D., Dickmann, F., & Kuchinke, L. (2018a). Reduction of Map Information Regulates Visual Attention without Affecting Route Recognition Performance. International Journal of Geo-Information, 7(12), 1–13. https://doi.org/10.3390/ijgi7120469

Keil, J., Edler, D., Dickmann, F., & Kuchinke, L. (2018b). Ambiguity of landmark pictograms enhances salience and recognition performance. Poster presented at the 7th International Conference on Spatial Cognition, Rome, Italy.

Keil, J., Edler, D., Dickmann, F., & Kuchinke, L. (2019a). Meaningfulness of landmark pictograms reduces visual salience and recognition performance. Applied Ergonomics, 75, 214–220. https://doi.org/10.1016/j.apergo.2018.10.008

Keil, J., Edler, D., Kuchinke, L., Dickmann, F. (2019b). Task-Oriented Display of Landmark Pictograms in Maps. Abstracts of the International Cartographic Association, 1, 167. https://doi.org/10.5194/ica-abs-1-167-2019

Keil, J., Edler, D., Kuchinke, L., & Dickmann, F. (2019c). Effects of visual map complexity on the attentional processing of landmarks. Manuscript submitted for publication.

References
--------

Anacta, V. J. A., Schwering, A., Li, R., & Muenzer, S. (2017). Orientation information in wayfinding instructions: Evidences from human verbal and visual instructions. GeoJournal, 82(3), 567–583. https://doi.org/10.1007/s10708-016-9703-5

Cipeluch, B., Jacob, R., Mooney, P., & Winstanley, A. C. (2010). Comparison of the accuracy of OpenStreetMap for Ireland with Google Maps and Bing Maps. In N. J. Tate & P. F. Fisher (Eds.), Proceedings of the Ninth International Symposium on Spatial Accuracy Assessment in Natural Resuorces and Enviromental Sciences (pp. 337–340). Leichester: University of Leichester.

Colby, G., & Scholl, L. (1991). Transparency and blur as selective cues for complex visual information. In W. R. Bender & W. Plouffe (Eds.), SPIE Proceedings, Image Handling and Reproduction Systems Integration (pp. 114–125). SPIE. https://doi.org/10.1117/12.44415

Dickmann, F., Edler, D., Bestgen, A.‑K., & Kuchinke, L. (2016). Exploiting Illusory Grid Lines for Object-Location Memory Performance in Urban Topographic Maps. The Cartographic Journal, 54(3), 242–253. https://doi.org/10.1080/00087041.2016.1236509

Elias, B., & Paelke, V. (2008). User-Centered Design of Landmark Visualizations. In L. Meng, A. Zipf, & S. Winter (Eds.), Map-based Mobile Services (pp. 33–56). Berlin, Heidelberg: Springer. https://doi.org/10.1007/978-3-540-37110-6_3

Foo, P., Warren, W. H., Duchon, A., & Tarr, M. J. (2005). Do humans integrate routes into a cognitive map? Map- versus landmark-based navigation of novel shortcuts. Journal of Experimental Psychology. Learning, Memory, and Cognition, 31(2), 195–215. https://doi.org/10.1037/0278-7393.31.2.195

Golledge, R. G. (1999). Human Wayfinding and Cognitive Maps. In R. G. Golledge (Ed.), Wayfinding Behavior: Cognitive Mapping and Other Spatial Processes (pp. 5–45). Baltimore, London: The Johns Hopkins University Press.

Grant, E. R., & Spivey, M. J. (2003). Eye movements and problem solving: Guiding attention guides thought. Psychological Science, 14(5), 462–466. https://doi.org/10.1111/1467-9280.02454

Haklay, M. (2010). How Good is Volunteered Geographical Information?: A Comparative Study of OpenStreetMap and Ordnance Survey Datasets. Environment and Planning B: Planning and Design, 37(4), 682–703. https://doi.org/10.1068/b35097

Just, M. A., & Carpenter, P. A. (1976). Eye fixations and cognitive processes. Cognitive Psychology, 8(4), 441–480. https://doi.org/10.1016/0010-0285(76)90015-3

McNamara, T. P., & Valiquette, C. M. (2004). Remembering Where Things Are. In G. L. Allen (Ed.), Human spatial memory: Remembering where. Mahwah, NJ: Erlbaum. https://doi.org/10.1002/acp.1179

Millonig, A., & Schechtner, K. (2007). Developing Landmark-Based Pedestrian-Navigation Systems. IEEE Transactions on Intelligent Transportation Systems, 8(1), 43–49. https://doi.org/10.1109/TITS.2006.889439

Sorrows, M. E., & Hirtle, S. C. (1999). The Nature of Landmarks for Real and Electronic Spaces. In C. Freksa & D. M. Mark (Eds.), Spatial Information Theory. Cognitive and Computational Foundations of Geographic Information Science: International Conference on Spatial Information Theory (pp. 37–50). Berlin, Heidelberg: Springer. https://doi.org/10.1007/3-540-48384-5_3

Sutherland, M. R., McQuiggan, D. A., Ryan, J. D., & Mather, M. (2017). Perceptual salience does not influence emotional arousal's impairing effects on top-down attention. Emotion (Washington, D.C.), 17(4), 700–706. https://doi.org/10.1037/emo0000245

Tversky, B. (2003). Structures of Mental Spaces: How People Think About Space. Environment and Behavior, 35(1), 66–80. https://doi.org/10.1177/0013916502238865


