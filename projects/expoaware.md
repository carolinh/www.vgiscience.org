---
layout: page
---

# ExpoAware – Environmental volunteered geographic information for personal exposure awareness and healthy mobility behaviour

**Prof. Dr. Uwe Schlink** 
*Helmholtz Centre for Environmental Research UFZ*,  
**Dr. Torsten Masson** 
*Institute of Psychology, University of Leipzig*

Urban spaces are hotspots of environmental pollution such as noise, air pollution and heat. These stressors affect our health and are highly contextually distributed in space and time. Portable sensors have advanced the measurement of these stressors and allow mobile exposure measurements.

Environmental-related Volunteered Geographic Information (VGI), which is actively collected by non-experts, can thus simultaneously provide valuable knowledge about temporal-spatial exposures and the mobility behaviour of individuals. Smartphone-based detection methods also provide active options to capture users' perceptions and opinions about exposure. This combination of subjective and objective VGI is a comprehensive data pool that not only allows exposure to be evaluated but also the behaviour and decision motives of individuals during everyday mobility.

In this project, we therefore work on three main questions: 1) How can environmental VGI generated with smartphones be used to extract spatiotemporal movement patterns and exposures? 2) How can we collect subjective data and analyse behavioural changes when confronting people with exposure data? 3) How can environmental VGI be used to improve mobility and exposure models?

We use intelligent sensors and an interventional study design, based on a self-developed framework of contemporary geography and psychological theories, to collect VGI with individuals from different socio-economic contexts. We will analyse the data using GIS-based methods of space-time analysis and psychological test statistics. To compare empirical data with models, we perform agent-based simulations.

The main goal of this project is to explore the potential of environmental VGI to change mobility behaviour in line with a healthier pathway decision by raising awareness of exposure.
